package eu.bernardoalvarez.simplepaymentservice.apierror;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class ErrorResponse {
    private Integer code;
    private String message;
    private String path;
    private ErrorResponse innerError;
}
