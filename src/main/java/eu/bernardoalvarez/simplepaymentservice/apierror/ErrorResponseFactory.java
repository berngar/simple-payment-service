package eu.bernardoalvarez.simplepaymentservice.apierror;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;
import java.util.Optional;

public class ErrorResponseFactory {
    public static ResponseEntity<ErrorResponse> createError(Throwable throwable, HttpStatus status, String path) {
        ErrorResponse response = ErrorResponse.builder()
                .code(status.value())
                .message(throwable.getMessage())
                .path(path)
                .innerError(getInnerException(throwable.getCause()))
                .build();
        return new ResponseEntity<>(response, status);
    }

    private static ErrorResponse getInnerException(Throwable throwable) {
        return Optional.ofNullable(throwable)
                .filter(Objects::nonNull)
                .map(error -> error.getMessage())
                .map(message -> ErrorResponse.builder()
                        .message(message)
                        .build())
                .orElse(null);
    }

    public static ResponseEntity<ErrorResponse> createNotFound(Throwable throwable, String path) {
        return createError(throwable, HttpStatus.NOT_FOUND, path);
    }

    public static ResponseEntity<ErrorResponse> createBadRequest(Throwable throwable, String path) {
        return createError(throwable, HttpStatus.BAD_REQUEST, path);
    }

    public static ResponseEntity<ErrorResponse> createPaymentRequired(Throwable throwable, String path) {
        return createError(throwable, HttpStatus.PAYMENT_REQUIRED, path);
    }

    public static ResponseEntity<ErrorResponse> createInternalServerError(Throwable throwable, String path) {
        return createError(throwable, HttpStatus.INTERNAL_SERVER_ERROR, path);
    }
}
