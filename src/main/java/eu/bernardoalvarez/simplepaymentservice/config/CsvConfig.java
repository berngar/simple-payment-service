package eu.bernardoalvarez.simplepaymentservice.config;

import org.apache.commons.csv.CSVFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class CsvConfig {
    @Bean
    public Writer csvFileWriter(@Value("${payments.export.csv.location}") Resource csv) throws IOException {
        if (!csv.exists() || !csv.isFile()) {
            csv.getFile().createNewFile();
        }
        return Files.newBufferedWriter(Paths.get(csv.getURI()));
    }

    @Bean
    public CSVFormat csvFormat() {
        return CSVFormat.DEFAULT.withHeader("paymentId", "clientId", "amount", "currency", "bankAccount");
    }
}
