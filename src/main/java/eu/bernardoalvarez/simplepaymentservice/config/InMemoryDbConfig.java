package eu.bernardoalvarez.simplepaymentservice.config;

import eu.bernardoalvarez.simplepaymentservice.payment.repository.PaymentRepository;
import eu.bernardoalvarez.simplepaymentservice.payment.repository.PaymentRepositoryInMemoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;


@Configuration
public class InMemoryDbConfig {
    @Bean
    @Profile("local")
    public PaymentRepository paymentDaoInMemory() {
        return new PaymentRepositoryInMemoryImpl(new ConcurrentHashMap<>(), new AtomicLong( 1L));
    }
}
