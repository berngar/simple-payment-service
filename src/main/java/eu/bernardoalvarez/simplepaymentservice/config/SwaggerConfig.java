package eu.bernardoalvarez.simplepaymentservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.Docket;

import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@Import({BeanValidatorPluginsConfiguration.class})
public class SwaggerConfig {
    @Value("${swagger.apiInfo.title}")
    private String title;

    @Value("${swagger.apiInfo.description}")
    private String description;

    @Value("${swagger.apiInfo.version}")
    private String version;

    @Bean
    public Docket docket() {
        return new Docket(SWAGGER_2).apiInfo(getApiInfo())
                .select()
                .apis(basePackage("eu.bernardoalvarez.simplepaymentservice"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder().title(title)
                .description(description)
                .version(version)
                .build();
    }
}
