package eu.bernardoalvarez.simplepaymentservice.exception;


public class BadRequestException extends RuntimeException {
    public static final String MESSAGE = "The request has an invalid or unknown format";

    public BadRequestException(Throwable throwable) {
        super(MESSAGE, throwable);
    }

    public BadRequestException(String message) {
        super(message);
    }
}
