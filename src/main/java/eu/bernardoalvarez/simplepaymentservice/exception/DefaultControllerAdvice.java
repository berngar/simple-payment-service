package eu.bernardoalvarez.simplepaymentservice.exception;

import eu.bernardoalvarez.simplepaymentservice.apierror.ErrorResponse;
import eu.bernardoalvarez.simplepaymentservice.apierror.ErrorResponseFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.badRequest;
import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.paymentRequired;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class DefaultControllerAdvice {
    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler({BadRequestException.class, MethodArgumentTypeMismatchException.class, HttpMessageNotReadableException.class})
    public ResponseEntity<ErrorResponse> onBadRequest(Exception e, ServletWebRequest request) {
        log.error("onBadRequest exception", e);
        if (e instanceof BadRequestException) {
            return ErrorResponseFactory.createBadRequest(e, request.getRequest().getRequestURI());
        }
        return ErrorResponseFactory.createBadRequest(badRequest(e), request.getRequest().getRequestURI());
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ErrorResponse> onNotFound(Exception e, ServletWebRequest request) {
        log.error("onNotFound exception", e);
        return ErrorResponseFactory.createNotFound(e, request.getRequest().getRequestURI());
    }

    @ResponseStatus(PAYMENT_REQUIRED)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> onPaymentRequired(Exception e, ServletWebRequest request) {
        log.error("onPaymentRequired exception", e);
        return ErrorResponseFactory.createPaymentRequired(paymentRequired(e), request.getRequest().getRequestURI());
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponse> onInternalServerError(Exception e, ServletWebRequest request) {
        log.error("onInternalServerError exception", e);
        return ErrorResponseFactory.createInternalServerError(e, request.getRequest().getRequestURI());
    }
}
