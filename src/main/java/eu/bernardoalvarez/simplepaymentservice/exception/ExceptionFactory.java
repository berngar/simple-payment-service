package eu.bernardoalvarez.simplepaymentservice.exception;

public class ExceptionFactory {
    public static RuntimeException badRequest(Throwable throwable) {
        return new BadRequestException(throwable);
    }

    public static RuntimeException badRequest(String message) {
        return new BadRequestException(message);
    }

    public static RuntimeException notFound() {
        return new NotFoundException();
    }

    public static RuntimeException paymentRequired(Throwable throwable) {
        return new PaymentRequiredException(throwable);
    }
}
