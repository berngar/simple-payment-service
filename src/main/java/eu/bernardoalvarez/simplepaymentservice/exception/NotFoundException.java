package eu.bernardoalvarez.simplepaymentservice.exception;

public class NotFoundException extends RuntimeException {
    public static final String MESSAGE = "The resource does not exist on the server";

    public NotFoundException() {
        super(MESSAGE);
    }
}
