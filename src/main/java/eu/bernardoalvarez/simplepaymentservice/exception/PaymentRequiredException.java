package eu.bernardoalvarez.simplepaymentservice.exception;

public class PaymentRequiredException extends RuntimeException {
    public static final String MESSAGE = "The request must contain a valid Payment";
    public PaymentRequiredException(Throwable throwable) {
        super(MESSAGE, throwable);
    }
}
