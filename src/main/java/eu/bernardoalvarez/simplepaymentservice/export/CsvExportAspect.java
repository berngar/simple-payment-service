package eu.bernardoalvarez.simplepaymentservice.export;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class CsvExportAspect {

    private final ExportService csvExportService;

    @After("execution(* eu.bernardoalvarez.simplepaymentservice.payment.repository.PaymentRepository.save(..))")
    public void saveToCsv(JoinPoint joinPoint) {
        Optional<Object> payment = Optional.ofNullable(joinPoint.getArgs())
                .filter(Objects::nonNull)
                .map(args -> Arrays.stream(args)
                        .filter(parameter -> parameter instanceof Payment)
                        .findFirst())
                .orElse(Optional.empty());

        payment.ifPresent(this::exportToCsv);
    }

    private void exportToCsv(Object object) {
        try {
            log.debug("Exporting to CSV a new entry: {}", object);
            csvExportService.save(object);
        } catch (Exception e) {
            log.error("Error exporting to CSV:", e);
        }
    }
}
