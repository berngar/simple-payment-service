package eu.bernardoalvarez.simplepaymentservice.export;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.springframework.stereotype.Service;

import java.io.Writer;

@Service
@RequiredArgsConstructor
public class CsvExportServiceImpl implements ExportService<Payment> {
    private final Writer csvWriter;
    private final CSVFormat csvFormat;

    @Override
    public void save(Payment element) throws Exception {
        synchronized (this) {
            csvFormat.printRecord(csvWriter, element.getPaymentId(), element.getClientId(), element.getAmount(), element.getCurrency(), element.getBankAccount());
            csvWriter.flush();
        }
    }
}
