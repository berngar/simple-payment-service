package eu.bernardoalvarez.simplepaymentservice.export;

public interface ExportService<T> {
    void save(T element) throws Exception;
}
