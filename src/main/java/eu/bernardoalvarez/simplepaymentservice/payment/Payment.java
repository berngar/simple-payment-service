package eu.bernardoalvarez.simplepaymentservice.payment;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonPropertyOrder({"paymentId", "clientId", "amount", "currency", "bankAccount"})
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private Long paymentId;

    @EqualsAndHashCode.Include
    private Long clientId;

    private BigDecimal amount;
    private String currency;
    private String bankAccount;

    public Payment(Long paymentId, Long clientId, BigDecimal amount, String currency, String bankAccount) {
        this.paymentId = paymentId;
        this.clientId = clientId;
        this.amount = amount;
        this.currency = currency;
        this.bankAccount = bankAccount;
    }

    public Payment(Long clientId, BigDecimal amount, String currency, String bankAccount) {
        this(0L, clientId, amount, currency, bankAccount);
    }
}
