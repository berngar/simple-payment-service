package eu.bernardoalvarez.simplepaymentservice.payment;

import java.math.BigDecimal;

public class PaymentFactory {
    public static Payment createPayment(long clientId, PaymentRequest paymentRequest) {
        return new Payment(paymentRequest.getPaymentId(), clientId, paymentRequest.getAmount(), paymentRequest.getCurrency(), paymentRequest.getBankAccount());
    }

    public static Payment createPayment(long paymentId, long clientId, PaymentRequest paymentRequest) {
        return new Payment(paymentId, clientId, paymentRequest.getAmount(), paymentRequest.getCurrency(), paymentRequest.getBankAccount());
    }

    public static Payment createPayment(String paymentId, String clientId, String amount, String currency, String bankAccount) {
        return new Payment(Long.decode(paymentId), Long.decode(clientId), new BigDecimal(amount), currency, bankAccount);
    }
}
