package eu.bernardoalvarez.simplepaymentservice.payment;

import eu.bernardoalvarez.simplepaymentservice.payment.controller.PaymentController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PaymentModelAssembler extends RepresentationModelAssemblerSupport<Payment, PaymentResponse> {

    public PaymentModelAssembler() {
        super(PaymentController.class, PaymentResponse.class);
    }

    @Override
    public PaymentResponse toModel(Payment entity) {
        PaymentResponse response = new PaymentResponse(entity);
        response.add(linkTo(methodOn(PaymentController.class).getPayment(entity.getClientId(), entity.getPaymentId()))
                .withSelfRel());
        return response;
    }
}
