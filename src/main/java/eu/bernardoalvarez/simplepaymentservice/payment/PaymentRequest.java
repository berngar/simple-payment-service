package eu.bernardoalvarez.simplepaymentservice.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest {
    private Long paymentId;

    @NotNull(message = "amount is mandatory")
    @DecimalMin(value = "0.0", inclusive = false, message = "minimum amount is 0.1")
    private BigDecimal amount;

    @NotBlank(message = "currency is mandatory")
    private String currency;

    @NotBlank(message = "bankAccount is mandatory")
    private String bankAccount;
}
