package eu.bernardoalvarez.simplepaymentservice.payment;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class PaymentResponse extends RepresentationModel<PaymentResponse> {
    private final Payment payment;
}
