package eu.bernardoalvarez.simplepaymentservice.payment.controller;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequest;
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentResponse;
import eu.bernardoalvarez.simplepaymentservice.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

import static eu.bernardoalvarez.simplepaymentservice.payment.controller.PaymentController.BASE_API_URL;

@RestController
@RequiredArgsConstructor
@RequestMapping(BASE_API_URL)
public class PaymentController {

    public static final String BASE_API_URL = "/api/v1/client/{clientId}";
    public static final String PAYMENT_RESOURCE = "/payment";
    public static final String PAYMENT_RESOURCE_ID = "/payment/{paymentId}";

    private final PaymentService paymentService;
    private final RepresentationModelAssemblerSupport<Payment, PaymentResponse> assembler;
    private final PagedResourcesAssembler<Payment> pagedResourcesAssembler;

    @GetMapping(path = PAYMENT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PagedModel<PaymentResponse>> getAllPayments(@PathVariable("clientId") long clientId,
                                                                      Pageable page) {
        Page<Payment> payments = paymentService.getAllPayments(clientId, page);
        PagedModel<PaymentResponse> response = pagedResourcesAssembler.toModel(payments, assembler);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = PAYMENT_RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponse> getPayment(@PathVariable("clientId") long clientId,
                                                      @PathVariable("paymentId") long paymentId) {
        Payment payment = paymentService.getPayment(clientId, paymentId);
        PaymentResponse response = assembler.toModel(payment);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = PAYMENT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addPayment(@PathVariable("clientId") long clientId,
                                             @Valid @RequestBody PaymentRequest paymentRequest) {
        Payment payment = paymentService.addPayment(clientId, paymentRequest);
        PaymentResponse response = assembler.toModel(payment);
        return response.getLinks()
                .stream()
                .map(Link::getHref)
                .map(URI::create)
                .map(ResponseEntity::created)
                .map(ResponseEntity.BodyBuilder::build)
                .findFirst()
                .orElseGet(() -> ResponseEntity.created(URI.create("")).build());
    }

    @PutMapping(path = PAYMENT_RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponse> updatePaymentByPaymentId(@PathVariable("clientId") long clientId,
                                                         @PathVariable("paymentId") long paymentId,
                                                         @Valid @RequestBody PaymentRequest paymentRequest) {
        Payment payment = paymentService.updatePayment(clientId, paymentId, paymentRequest);
        PaymentResponse response = assembler.toModel(payment);
        return ResponseEntity.ok(response);
    }

    @PutMapping(path = PAYMENT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponse> updatePayment(@PathVariable("clientId") long clientId,
                                                         @Valid @RequestBody PaymentRequest paymentRequest) {
        Payment payment = paymentService.updatePayment(clientId, paymentRequest);
        PaymentResponse response = assembler.toModel(payment);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(path = PAYMENT_RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deletePayment(@PathVariable("clientId") long clientId,
                                                @PathVariable("paymentId") long paymentId) {
        paymentService.deletePayment(clientId, paymentId);
        return ResponseEntity.noContent()
                .build();
    }
}
