package eu.bernardoalvarez.simplepaymentservice.payment.repository;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface PaymentRepository extends Repository<Payment, Long> {
    Payment save(Payment payment);

    @Query("select p from Payment p where p.clientId = :clientId")
    Iterable<Payment> findAllByClientId(@Param("clientId") Long clientId);

    Page<Payment> findAllByClientId(@Param("clientId") Long clientId, Pageable pageable);

    @Query("select p from Payment p where p.clientId = :clientId and p.paymentId = :paymentId")
    Optional<Payment> findByClientIdAndPaymentId(@Param("clientId") Long clientId, @Param("paymentId") Long paymentId);

    @Modifying
    @Transactional
    @Query("delete from Payment p where p.clientId = :clientId and p.paymentId = :paymentId")
    void delete(@Param("clientId") Long clientId, @Param("paymentId") Long paymentId);
}
