package eu.bernardoalvarez.simplepaymentservice.payment.repository;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RequiredArgsConstructor
public class PaymentRepositoryInMemoryImpl implements PaymentRepository {

    private final Map<Long, Set<Payment>> inMemoryPayments;
    private final AtomicLong index;

    @Override
    public Payment save(Payment payment) {
        inMemoryPayments.putIfAbsent(payment.getClientId(), new CopyOnWriteArraySet<>());
        Set<Payment> payments = inMemoryPayments.get(payment.getClientId());
        if (payments.contains(payment)) {
            payments.remove(payment);
        } else {
            payment.setPaymentId(index.getAndIncrement());
        }
        payments.add(payment);

        return payment;
    }

    @Override
    public Iterable<Payment> findAllByClientId(Long clientId) {
        return getAllPaymentsByClientId(clientId);
    }

    @Override
    public Page<Payment> findAllByClientId(Long clientId, Pageable pageable) {
        List<Payment> payments = getAllPaymentsByClientId(clientId);
        long maxLastElement = pageable.getOffset() + pageable.getPageSize();
        int firstElement = (int) Math.min(pageable.getOffset(), payments.size());
        int lastElement = (int) Math.min(maxLastElement, payments.size());

        return new PageImpl<>(payments.subList(firstElement, lastElement), pageable, payments.size());
    }

    private List<Payment> getAllPaymentsByClientId(Long clientId) {
        List<Payment> payments = new ArrayList<>();
                inMemoryPayments.getOrDefault(clientId, new HashSet<>())
                .iterator()
                .forEachRemaining(payments::add);
        return payments;
    }

    @Override
    public Optional<Payment> findByClientIdAndPaymentId(Long clientId, Long paymentId) {
        Optional<Payment> payment = Optional.empty();
        Set<Payment> payments = inMemoryPayments.getOrDefault(clientId, new HashSet<>());

        for (Payment element : payments) {
            if (element.getPaymentId() == paymentId) {
                log.debug("Retrieved paymentId {} for clientId {}", paymentId, clientId);
                payment = Optional.ofNullable(element);
                break;
            }
        }

        return payment;
    }

    @Override
    public void delete(Long clientId, Long paymentId) {
        Optional.ofNullable(inMemoryPayments.get(clientId))
                .map(payments -> payments.removeIf((element -> element.getPaymentId() == paymentId)));
    }
}
