package eu.bernardoalvarez.simplepaymentservice.payment.service;

import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentService {
    Payment addPayment(long clientId, PaymentRequest paymentRequest);
    Page<Payment> getAllPayments(long clientId, Pageable page);
    Payment getPayment(long clientId, long paymentId);
    void deletePayment(long clientId, long paymentId);
    Payment updatePayment(long clientId, PaymentRequest paymentRequest);
    Payment updatePayment(long clientId, long paymentId, PaymentRequest paymentRequest);
}
