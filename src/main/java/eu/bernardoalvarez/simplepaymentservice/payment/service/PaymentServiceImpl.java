package eu.bernardoalvarez.simplepaymentservice.payment.service;

import eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory;
import eu.bernardoalvarez.simplepaymentservice.payment.Payment;
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentFactory;
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequest;
import eu.bernardoalvarez.simplepaymentservice.payment.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.badRequest;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private static final String MISSING_PAYMENT_ID = "Missing paymentId";
    private final PaymentRepository paymentRepository;

    @Override
    public Payment addPayment(long clientId, PaymentRequest paymentRequest) {
        Payment payment = PaymentFactory.createPayment(clientId, paymentRequest);
        log.debug("Saving a Payment {}", payment);
        return paymentRepository.save(payment);
    }

    @Override
    public Page<Payment> getAllPayments(long clientId, Pageable page) {
        log.debug("Retrieving all Payments for clientId {}", clientId);
        return paymentRepository.findAllByClientId(clientId, page);
    }

    @Override
    public Payment getPayment(long clientId, long paymentId) {
        log.debug("Retrieving paymentId {} for clientId {}", paymentId, clientId);
        return paymentRepository.findByClientIdAndPaymentId(clientId, paymentId)
                .orElseThrow(ExceptionFactory::notFound);
    }

    @Override
    public void deletePayment(long clientId, long paymentId) {
        paymentRepository.delete(clientId, paymentId);
    }

    @Override
    public Payment updatePayment(long clientId, PaymentRequest paymentRequest) {
        log.debug("Updating paymentId {} for clientId {}", paymentRequest.getPaymentId(), clientId);
        Long paymentId = Optional.ofNullable(paymentRequest.getPaymentId())
                .orElseThrow(() -> badRequest(MISSING_PAYMENT_ID));
        paymentRepository.findByClientIdAndPaymentId(clientId, paymentId)
                .orElseThrow(ExceptionFactory::notFound);
        Payment updatedPayment = PaymentFactory.createPayment(paymentId, clientId, paymentRequest);
        return paymentRepository.save(updatedPayment);
    }

    @Override
    public Payment updatePayment(long clientId, long paymentId, PaymentRequest paymentRequest) {
        log.debug("Updating paymentId {} for clientId {}", paymentId, clientId);
        paymentRepository.findByClientIdAndPaymentId(clientId, paymentId)
                .orElseThrow(ExceptionFactory::notFound);
        Payment updatedPayment = PaymentFactory.createPayment(paymentId, clientId, paymentRequest);
        return paymentRepository.save(updatedPayment);
    }
}
