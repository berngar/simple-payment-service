package eu.bernardoalvarez.simplepaymentservice.apierror

import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.apierror.ErrorResponseFactory.createError
import static eu.bernardoalvarez.simplepaymentservice.apierror.ErrorResponseFactory.getInnerException
import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.*
import static org.springframework.http.HttpStatus.*

class ErrorResponseFactoryTest extends Specification {

    def static NOTFOUND_MESSAGE = 'The resource does not exist on the server'
    def static BADREQUEST_MESSAGE = 'The request has an invalid or unknown format'
    def static PAYMENTREQUIRED_MESSAGE = 'The request must contain a valid Payment'


    @Unroll
    Should "create an ErrorResponse with #description"() {
        when:
        def result = createError(throwable, status, path)
        then:
        result.body.code == status.value()
        result.body.message == message
        result.body.path == path
        result.body?.innerError?.code == null
        result.body?.innerError?.message == innerMessage
        result.body?.innerError?.path == null
        where:
        message                 | throwable                  | innerMessage     | status      | path | description
        'test'                  | badRequest(message)        | null             | BAD_REQUEST | '/'  | 'bad request exception and no inner error'
        NOTFOUND_MESSAGE        | notFound()                 | null             | NOT_FOUND   | '/'  | 'not found exception and no inner error'
        BADREQUEST_MESSAGE      | badRequest(notFound())     | NOTFOUND_MESSAGE | BAD_REQUEST | '/'  | 'bad request exception with inner not found exception'
        PAYMENTREQUIRED_MESSAGE | paymentRequired(notFound()) | NOTFOUND_MESSAGE | BAD_REQUEST | '/' | 'payment required exception with inner not found exception'
    }

    Should "get an inner ErrorResponse with message when the exception is not null"() {
        given:
        def exception = Stub(Throwable)
        exception.message >> 'test'
        when:
        def result = getInnerException(exception)
        then:
        result.code == null
        result.path == null
        result.innerError == null
        result.message == 'test'
    }

    Should "get a null inner ErrorResponse when the exception is null"() {
        when:
        def result = getInnerException(null)
        then:
        result == null
    }

    @Unroll
    Should "create a ResponseEntity with a #description"() {
        given:
        def exception = new RuntimeException(message)
        when:
        def result = ErrorResponseFactory."$factoryMethod"(exception, path)
        then:
        result.body.code == status.value()
        result.body.message == message
        result.body.path == path
        where:
        factoryMethod               | message | status                | path | description
        'createBadRequest'          | 'test'  | BAD_REQUEST           | '/'  | 'Bad Request'
        'createNotFound'            | 'test'  | NOT_FOUND             | '/'  | 'Not Found'
        'createPaymentRequired'     | 'test'  | PAYMENT_REQUIRED      | '/'  | 'Payment Required'
        'createInternalServerError' | 'test'  | INTERNAL_SERVER_ERROR | '/'  | 'Internal Server Error'
    }
}
