package eu.bernardoalvarez.simplepaymentservice.exception

import org.springframework.web.context.request.ServletWebRequest
import spock.lang.Specification
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest
import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.badRequest
import static eu.bernardoalvarez.simplepaymentservice.exception.ExceptionFactory.notFound
import static org.springframework.http.HttpStatus.*

class DefaultControllerAdviceTest extends Specification {
    def controllerAdvice

    def setup() {
        controllerAdvice = new DefaultControllerAdvice()
    }

    @Unroll
    Should "create a ResponseEntity with #description"() {
        given:
        def httpServletRequest = Stub(HttpServletRequest)
        def servletRequest = new ServletWebRequest(httpServletRequest)
        httpServletRequest.requestURI >> '/'
        when:
        def result = controllerAdvice."$controllerMethod"(exception, servletRequest)
        then:
        result.body.code == status.value()
        result.body.message == message
        result.body.path == '/'
        result.body?.innerError?.code == null
        result.body?.innerError?.message == innerMessage
        result.body?.innerError?.path == null
        where:
        controllerMethod        | message                          | exception                     | status                | innerMessage              | description
        'onBadRequest'          | 'test message'                   | badRequest(message)           | BAD_REQUEST           | null                      | 'Bad Request with no innerMessage'
        'onBadRequest'          | BadRequestException.MESSAGE      | badRequest(notFound())        | BAD_REQUEST           | NotFoundException.MESSAGE | 'Bad Request with innerMessage'
        'onBadRequest'          | BadRequestException.MESSAGE      | new RuntimeException(message) | BAD_REQUEST           | message                   | 'Bad Request (not triggered by BadRequestException) with no innerMessage'
        'onNotFound'            | NotFoundException.MESSAGE        | notFound()                    | NOT_FOUND             | null                      | 'Not Found with no innerMessage'
        'onPaymentRequired'     | PaymentRequiredException.MESSAGE | notFound()                    | PAYMENT_REQUIRED      | NotFoundException.MESSAGE | 'Payment Required with  innerMessage'
        'onInternalServerError' | 'test message'                   | new RuntimeException(message) | INTERNAL_SERVER_ERROR | null                      | 'Internal Server Error with no innerMessage'
    }
}
