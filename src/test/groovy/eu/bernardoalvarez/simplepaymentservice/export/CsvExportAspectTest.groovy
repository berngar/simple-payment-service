package eu.bernardoalvarez.simplepaymentservice.export

import org.aspectj.lang.JoinPoint
import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment

class CsvExportAspectTest extends Specification {

    def aspect
    def csvExportService

    def setup() {
        csvExportService = Mock(CsvExportServiceImpl)
        aspect = new CsvExportAspect(csvExportService)
    }

    @Unroll
    Should "#action to CSV when #description"() {
        given:
        def joinPoint = Mock(JoinPoint)
        joinPoint.getArgs() >> joinPointArgs
        when:
        aspect.saveToCsv(joinPoint)
        then:
        cardinality * csvExportService.save(object)
        where:
        cardinality | object         | joinPointArgs    | action       | description
        0           | null           | null             | 'not export' | 'there args is null'
        0           | null           | []               | 'not export' | 'there are no args'
        0           | null           | [null]           | 'not export' | 'there are is one null in args'
        1           | getPayment(1L) | [object]         | 'export'     | 'there are is one Payment in args'
        1           | getPayment(1L) | [object, 'test'] | 'export'     | 'there are is one Payment and an extra object in args'
        1           | getPayment(1L) | [object, null]   | 'export'     | 'there are is one Payment and an extra null in args'
    }
}
