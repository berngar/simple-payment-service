package eu.bernardoalvarez.simplepaymentservice.export

import eu.bernardoalvarez.simplepaymentservice.payment.Payment
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentFactory
import org.apache.commons.csv.CSVRecord

class CsvExportServiceDsl {
    static Payment getPayment(CSVRecord record) {
        String paymentId = record.get("paymentId");
        String clientId = record.get("clientId");
        String amount = record.get("amount");
        String currency = record.get("currency");
        String bankAccount = record.get("bankAccount");

        return PaymentFactory.createPayment(paymentId, clientId, amount, currency, bankAccount);
    }
}
