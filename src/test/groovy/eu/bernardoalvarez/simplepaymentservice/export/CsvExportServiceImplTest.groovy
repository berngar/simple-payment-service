package eu.bernardoalvarez.simplepaymentservice.export


import eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl
import org.apache.commons.csv.CSVFormat
import org.apache.commons.io.FileUtils
import spock.lang.Specification

import java.lang.Void as Should
import java.nio.file.Files
import java.nio.file.Paths

import static java.util.stream.Collectors.toList


class CsvExportServiceImplTest extends Specification {

    def exportService
    CSVFormat csvFormat
    def csvPath

    def setup() {
        csvPath = Paths.get(FileUtils.getTempDirectoryPath(), 'payment-test.csv')
        def csvWriter = Files.newBufferedWriter(csvPath)
        csvFormat = CSVFormat.DEFAULT.withHeader("paymentId", "clientId", "amount", "currency", "bankAccount");
        exportService = new CsvExportServiceImpl(csvWriter, csvFormat)
    }

    def cleanup() {
        Files.deleteIfExists(csvPath)
    }

    Should "save a Payment to a CSV file"() {
        given:
        def csvReader = Files.newBufferedReader(csvPath)
        def payment = PaymentDsl.getPayment(1L, 12345L)
        when:
        exportService.save(payment)
        def csvEntry = csvFormat.parse(csvReader)
                .records
                .stream()
                .map({ record -> CsvExportServiceDsl.getPayment(record) })
                .collect(toList())
                .first()
        then:
        payment == csvEntry
    }
}
