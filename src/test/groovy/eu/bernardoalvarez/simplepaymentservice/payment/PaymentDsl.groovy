package eu.bernardoalvarez.simplepaymentservice.payment

class PaymentDsl {
    static def getPayment(def clientId) {
        new Payment(clientId, new BigDecimal(12345), 'PLN', 'PL1234567890')
    }

    static def getPayment(def paymentId, def clientId) {
        new Payment(paymentId, clientId, new BigDecimal(12345), 'PLN', 'PL1234567890')
    }

    static def generatePayments(def clientId, def numPayments) {
        def payments = []
        (1 .. numPayments).each {paymentId ->
            payments << getPayment(paymentId, clientId)
        }
        payments
    }
}
