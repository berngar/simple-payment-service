package eu.bernardoalvarez.simplepaymentservice.payment

import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequestDsl.getPaymentRequest

class PaymentFactoryTest extends Specification {

    @Unroll
    Should "create a Payment object with clientId from a PaymentRequest when #description"() {
        given:
        def paymentRequest = getPaymentRequest(amount, currency, bankAccount)
        when:
        def payment = PaymentFactory.createPayment(clientId, paymentRequest)
        then:
        payment.clientId == clientId
        payment.amount == amount
        payment.currency == currency
        payment.bankAccount == bankAccount
        where:
        clientId | amount | currency | bankAccount        | description
        12345L   | 1L     | 'EUR'    | 'ES12345678910112' | 'all fields have valid values'
        56789L   | 1000L  | ''       | 'ES12345678910112' | 'currency is empty'
        1L       | 0L     | 'EUR'    | ''                 | 'bankAccount is empty'
    }

    @Unroll
    Should "create a Payment object with clientId and paymentId from a PaymentRequest when #description"() {
        given:
        def paymentRequest = getPaymentRequest(amount, currency, bankAccount)
        when:
        def payment = PaymentFactory.createPayment(paymentId, clientId, paymentRequest)
        then:
        payment.paymentId == paymentId
        payment.clientId == clientId
        payment.amount == amount
        payment.currency == currency
        payment.bankAccount == bankAccount
        where:
        paymentId | clientId | amount | currency | bankAccount        | description
        1L        | 12345L   | 1L     | 'EUR'    | 'ES12345678910112' | 'all fields have valid values'
        1L        | 56789L   | 1000L  | ''       | 'ES12345678910112' | 'currency is empty'
        1L        | 1L       | 0L     | 'EUR'    | ''                 | 'bankAccount is empty'
    }

    @Unroll
    Should "create a Payment object from Strings where #description"() {
        when:
        def payment = PaymentFactory.createPayment(paymentId, clientId, amount, currency, bankAccount)
        then:
        payment.paymentId == paymentId as long
        payment.clientId == clientId as long
        payment.amount == amount as BigDecimal
        payment.currency == currency
        payment.bankAccount == bankAccount
        where:
        paymentId | clientId | amount | currency | bankAccount        | description
        '1'       | '12345'  | '1'    | 'EUR'    | 'ES12345678910112' | 'all fields have valid values'
        '1'       | '56789'  | '1000' | ''       | 'ES12345678910112' | 'currency is empty'
        '1'       | '1'      | '0'    | 'EUR'    | ''                 | 'bankAccount is empty'
    }

}
