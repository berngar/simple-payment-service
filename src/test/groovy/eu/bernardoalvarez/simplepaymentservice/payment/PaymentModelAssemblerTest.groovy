package eu.bernardoalvarez.simplepaymentservice.payment

import spock.lang.Specification

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment

class PaymentModelAssemblerTest extends Specification {

    def assembler

    def setup() {
        assembler = new PaymentModelAssembler()
    }

    Should "create a PaymentResponse with self-link"() {
        given:
        def payment = getPayment(1L, 1L)
        when:
        def result = assembler.toModel(payment)
        then:
        result.payment == payment
        result.links*.href.first() == '/api/v1/client/1/payment/1'
    }
}
