package eu.bernardoalvarez.simplepaymentservice.payment

class PaymentRequestDsl {
    def static getPaymentRequest(def amount, def currency, def bankAccount) {
        def request = new PaymentRequest()
        request.amount = new BigDecimal(amount as long)
        request.currency = currency
        request.bankAccount = bankAccount

        request
    }

    def static getPaymentRequest(def paymentId, def amount, def currency, def bankAccount) {
        def request = new PaymentRequest()
        request.paymentId = paymentId
        request.amount = new BigDecimal(amount as long)
        request.currency = currency
        request.bankAccount = bankAccount

        request
    }
}
