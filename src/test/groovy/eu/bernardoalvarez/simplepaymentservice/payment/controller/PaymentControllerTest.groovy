package eu.bernardoalvarez.simplepaymentservice.payment.controller

import eu.bernardoalvarez.simplepaymentservice.payment.PaymentModelAssembler
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentResponse
import eu.bernardoalvarez.simplepaymentservice.payment.service.PaymentService
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedModel
import spock.lang.Specification

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment
import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequestDsl.getPaymentRequest
import static org.springframework.http.HttpStatus.*

class PaymentControllerTest extends Specification {
    def controller
    def paymentService
    def assembler
    def pagedAssembler

    def setup() {
        paymentService = Mock PaymentService
        assembler = new PaymentModelAssembler()
        pagedAssembler = Mock PagedResourcesAssembler
        controller = new PaymentController(paymentService, assembler, pagedAssembler)
    }

    Should "get all payments"() {
        given:
        def pageable = PageRequest.of(1, 1)
        def pagedModel = new PagedModel<PaymentResponse>()
        when:
        def response = controller.getAllPayments(1, pageable)
        then:
        1 * paymentService.getAllPayments(_, _)
        1 * pagedAssembler.toModel(_, _) >> pagedModel
        response.statusCode == OK
        response.body == pagedModel
    }

    Should "get a payment"() {
        given:
        def payment = getPayment(1, 1)
        paymentService.getPayment(1, 1) >> payment
        when:
        def response = controller.getPayment(1, 1)
        then:
        response.statusCode == OK
        response.body.payment.paymentId == payment.paymentId
        response.body.payment.clientId == payment.clientId
        response.body.payment.amount == payment.amount
        response.body.payment.currency == payment.currency
        response.body.payment.bankAccount == payment.bankAccount
    }

    Should "add a new payment"() {
        given:
        def payment = getPayment(1, 1)
        def paymentRequest = getPaymentRequest(payment.amount, payment.currency, payment.bankAccount)
        when:
        def response = controller.addPayment(1, paymentRequest)
        then:
        1 * paymentService.addPayment(1, paymentRequest) >> payment
        response.statusCode == CREATED
    }

    Should "update a payment by paymentId"() {
        given:
        def payment = getPayment(1, 1)
        def paymentRequest = getPaymentRequest(payment.amount, payment.currency, payment.bankAccount)
        when:
        def response = controller.updatePaymentByPaymentId(payment.paymentId, payment.clientId, paymentRequest)
        then:
        1 * paymentService.updatePayment(payment.paymentId, payment.clientId, paymentRequest) >> payment
        response.statusCode == OK
        response.body.payment == payment
    }

    Should "update a payment by PaymentRequest"() {
        given:
        def payment = getPayment(1, 1)
        def paymentRequest = getPaymentRequest(payment.amount, payment.currency, payment.bankAccount)
        when:
        def response = controller.updatePayment(payment.clientId, paymentRequest)
        then:
        1 * paymentService.updatePayment(payment.clientId, paymentRequest) >> payment
        response.statusCode == OK
        response.body.payment == payment
    }

    Should "delete a payment"() {
        when:
        def response = controller.deletePayment(1, 1)
        then:
        1 * paymentService.deletePayment(1, 1)
        response.statusCode == NO_CONTENT
    }
}
