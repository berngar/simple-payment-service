package eu.bernardoalvarez.simplepaymentservice.payment.repository


import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment

class PaymentRepositoryInMemoryDsl {
    def static flushOnlyIfFirstTest(def dao, def numTest) {
        if (numTest == 1) {
            dao = new PaymentRepositoryInMemoryImpl(new ConcurrentHashMap(), new AtomicLong(1L))
        }
        dao
    }

    static def saveMockPaymentsForClientId(def dao, def clientId, def numPayments) {
        if (numPayments > 0) {
            (1..numPayments).forEach({ payment ->
                def newPayment = getPayment(clientId)
                dao.save(newPayment)
            })
        }
    }
}
