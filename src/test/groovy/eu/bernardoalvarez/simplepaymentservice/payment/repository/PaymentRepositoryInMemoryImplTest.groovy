package eu.bernardoalvarez.simplepaymentservice.payment.repository

import eu.bernardoalvarez.simplepaymentservice.payment.Payment
import org.springframework.data.domain.PageRequest
import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

import static PaymentRepositoryInMemoryDsl.flushOnlyIfFirstTest
import static PaymentRepositoryInMemoryDsl.saveMockPaymentsForClientId
import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment

class PaymentRepositoryInMemoryImplTest extends Specification {

    static PaymentRepository paymentDaoInMemoryImpl

    def setupSpec() {
        paymentDaoInMemoryImpl = new PaymentRepositoryInMemoryImpl(new ConcurrentHashMap(), new AtomicLong(1L))
    }

    @Unroll
    Should "save a payment in-memory when #description"() {
        given:
        def payment = new Payment(clientId, amount, currency, bankAccount)
        when:
        def result = paymentDaoInMemoryImpl.save(payment)
        then:
        result == payment
        result.paymentId == expectedIndex
        where:
        clientId | amount                    | currency | bankAccount    | expectedIndex | description
        12345L   | new BigDecimal(123456789) | 'PLN'    | 'PL1234567890' | 1             | 'all fields have values'
        12345L   | null                      | 'PLN'    | 'PL1234567890' | 2             | 'when amount is null'
        12345L   | new BigDecimal(123456789) | null     | 'PL1234567890' | 3             | 'when currency is null'
        12345L   | new BigDecimal(123456789) | 'PLN'    | null           | 4             | 'when bankAccount is null'
        12345L   | new BigDecimal(0)         | 'PLN'    | 'PL1234567890' | 5             | 'when amount is zero'
        12345L   | new BigDecimal(123456789) | ''       | 'PL1234567890' | 6             | 'when currency is empty'
        12345L   | new BigDecimal(123456789) | 'PLN'    | ''             | 7             | 'when bankAccount is empty'
    }

    @Unroll
    Should "find #numPayments payments for #description"() {
        given:
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, numPayments)
        when:
        def result = paymentDaoInMemoryImpl.findAllByClientId(clientId)
        then:
        result.size() == numPayments
        where:
        clientId | numPayments | description
        11111L   | 6           | 'clientId 11111'
        99999L   | 0           | 'a non-existent clientId'
        67890L   | 1           | 'clientId 67890'
    }

    @Unroll
    Should "find #pageSize payments in page #numPage when the page size is #pageSize"() {
        given:
        paymentDaoInMemoryImpl = flushOnlyIfFirstTest(paymentDaoInMemoryImpl, numTest)
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, numPayments)
        def page = PageRequest.of(numPage, pageSize)
        when:
        def result = paymentDaoInMemoryImpl.findAllByClientId(clientId, page)
        then:
        result.totalElements == totalElements
        result.totalPages == totalPages
        result.content*.paymentId == paymentsInPage
        where:
        numTest | clientId | numPayments | pageSize | numPage | totalElements | totalPages | paymentsInPage
        1       | 12345L   | 6           | 2        | 0       | 6             | 3          | [1L, 2L]
        2       | 12345L   | 0           | 2        | 1       | 6             | 3          | [3L, 4L]
        3       | 12345L   | 0           | 2        | 2       | 6             | 3          | [5L, 6L]
        4       | 12345L   | 0           | 3        | 0       | 6             | 2          | [1L, 2L, 3L]
        5       | 12345L   | 0           | 3        | 1       | 6             | 2          | [4L, 5L, 6L]
        6       | 12345L   | 0           | 4        | 0       | 6             | 2          | [1L, 2L, 3L, 4L]
        7       | 12345L   | 0           | 4        | 1       | 6             | 2          | [5L, 6L]
        8       | 12345L   | 1           | 4        | 1       | 7             | 2          | [5L, 6L, 7L]
    }

    @Unroll
    Should "find a payment by clientId and paymentId where #description"() {
        given:
        paymentDaoInMemoryImpl = flushOnlyIfFirstTest(paymentDaoInMemoryImpl, numTest)
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, createNumPayments)
        when:
        Optional<Payment> result = paymentDaoInMemoryImpl.findByClientIdAndPaymentId(clientId, paymentId)
        def payment = result.get()
        then:
        result.present == true
        payment.paymentId == paymentId
        payment.clientId == clientId
        where:
        clientId | createNumPayments | paymentId | numTest | description
        22222L   | 1                 | 1         | 1       | 'clientId 22222 has one payment with paymentId 1'
        22222L   | 1                 | 2         | 2       | 'clientId 22222 has a payment with paymentId 2'
        33333L   | 1                 | 3         | 3       | 'clientId 33333 has a payment with paymentId 3'
        33333L   | 2                 | 5         | 4       | 'clientId 33333 has a payment with paymentId 5'
    }

    @Unroll
    Should "not find a payment by clientId and paymentId where #description"() {
        given:
        paymentDaoInMemoryImpl = flushOnlyIfFirstTest(paymentDaoInMemoryImpl, numTest)
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, createNumPayments)
        when:
        Optional<Payment> result = paymentDaoInMemoryImpl.findByClientIdAndPaymentId(searchClientId, paymentId)
        then:
        result.present == false

        where:
        clientId | searchClientId | createNumPayments | paymentId | numTest | description
        22222L   | 22222L         | 1                 | 0         | 1       | 'paymentId is invalid'
        22222L   | 22222L         | 0                 | 2         | 2       | 'paymentId is bigger than the last paymentId'
        22222L   | 44444L         | 0                 | 0         | 3       | 'paymentId is invalid and clientId does not exist'
        22222L   | 44444L         | 0                 | 2         | 4       | 'paymentId is bigger than the last paymentId and clientId does not exist'
        22222L   | 44444L         | 0                 | 1         | 5       | 'paymentId is valid but clientId does not exist'
    }

    @Unroll
    Should "delete a payment by clientId and paymentId where #description"() {
        given:
        paymentDaoInMemoryImpl = flushOnlyIfFirstTest(paymentDaoInMemoryImpl, numTest)
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, createNumPayments)
        when:
        paymentDaoInMemoryImpl.delete(searchClientId, paymentId)
        Optional<Payment> result = paymentDaoInMemoryImpl.findByClientIdAndPaymentId(searchClientId, paymentId)
        def allPayments = paymentDaoInMemoryImpl.findAllByClientId(searchClientId)
        def paymentIds = allPayments*.paymentId
        then:
        paymentIds.containsAll(expectedPaymentIds) == true
        result.present == false

        where:
        clientId | searchClientId | createNumPayments | paymentId | expectedNumPayments | expectedPaymentIds | numTest | description
        22222L   | 22222L         | 1                 | 0         | 1                   | [1L]               | 1       | 'paymentId is invalid'
        22222L   | 22222L         | 0                 | 2         | 1                   | [1L]               | 2       | 'paymentId is bigger than the last paymentId'
        22222L   | 22222L         | 0                 | 1         | 0                   | []                 | 3       | 'paymentId is valid'
        22222L   | 22222L         | 5                 | 3         | 4                   | [2L, 4L, 5L, 6L]   | 4       | 'paymentId is valid'
        22222L   | 22222L         | 5                 | 5         | 3                   | [2L, 4L, 6L]       | 4       | 'paymentId is valid'
        22222L   | 44444L         | 0                 | 0         | 0                   | []                 | 5       | 'paymentId is invalid and clientId does not exist'
        22222L   | 44444L         | 0                 | 2         | 0                   | []                 | 6       | 'paymentId is bigger than the last paymentId and clientId does not exist'
        22222L   | 44444L         | 0                 | 5         | 0                   | []                 | 7       | 'paymentId is valid but clientId does not exist'
    }

    @Unroll
    Should "update a payment where #description"() {
        given:
        paymentDaoInMemoryImpl = flushOnlyIfFirstTest(paymentDaoInMemoryImpl, numTest)
        saveMockPaymentsForClientId(paymentDaoInMemoryImpl, clientId, numPayments)
        def payment = getPayment(paymentId, clientId)
        payment."$paymentField" = fieldValue
        when:
        paymentDaoInMemoryImpl.save(payment)
        Optional<Payment> result = paymentDaoInMemoryImpl.findByClientIdAndPaymentId(clientId, paymentId)
        def allPayments = paymentDaoInMemoryImpl.findAllByClientId(clientId)
        def paymentIds = allPayments*.paymentId
        then:
        paymentIds.containsAll(expectedPaymentIds) == true
        result.present == true
        result.get() == payment

        where:
        clientId | numPayments | paymentId | paymentField  | fieldValue               | expectedPaymentIds | numTest | description
        22222L   | 2           | 1         | 'currency'    | 'EUR'                    | [1L, 2L]           | 1       | "updates $paymentField field for paymentId $paymentId"
        22222L   | 0           | 1         | 'amount'      | new BigDecimal(1)        | [1L, 2L]           | 2       | "updates $paymentField field for paymentId $paymentId"
        22222L   | 0           | 1         | 'bankAccount' | 'DE11111111111111111111' | [1L, 2L]           | 3       | "updates $paymentField field for paymentId $paymentId"
        22222L   | 0           | 3         | 'paymentId'   | 3                        | [1L, 2L, 3L]       | 4       | "updates $paymentField with a new paymentId and effectively creates a new payment"
    }
}
