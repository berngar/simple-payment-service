package eu.bernardoalvarez.simplepaymentservice.payment.service

import eu.bernardoalvarez.simplepaymentservice.exception.BadRequestException
import eu.bernardoalvarez.simplepaymentservice.exception.NotFoundException
import eu.bernardoalvarez.simplepaymentservice.payment.Payment
import eu.bernardoalvarez.simplepaymentservice.payment.PaymentFactory
import eu.bernardoalvarez.simplepaymentservice.payment.repository.PaymentRepository
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import spock.lang.Specification
import spock.lang.Unroll

import java.lang.Void as Should

import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentDsl.getPayment
import static eu.bernardoalvarez.simplepaymentservice.payment.PaymentRequestDsl.getPaymentRequest

class PaymentServiceTest extends Specification {
    def paymentRepository
    PaymentService paymentService

    def setup() {
        paymentRepository = Mock PaymentRepository
        paymentService = new PaymentServiceImpl(paymentRepository)
    }

    Should "add a payment"() {
        given:
        def paymentRequest = getPaymentRequest(1L, 'PLN', 'PLN1234567890123456789')
        def payment = PaymentFactory.createPayment(1L, paymentRequest)
        when:
        def result = paymentService.addPayment(payment.clientId, paymentRequest)
        then:
        1 * paymentRepository.save(_) >> payment
        result == payment
    }

    Should "get all payments"() {
        given:
        def payment = getPayment(1L)
        def page = new PageImpl<Payment>([payment])
        when:
        def result = paymentService.getAllPayments(payment.clientId, PageRequest.of(1, 1))
        then:
        1 * paymentRepository.findAllByClientId(_, _) >> page
        page.size == 1
        page.totalPages == 1
        page.first() == payment
    }

    Should "get a payment"() {
        given:
        def payment = getPayment(1L, 1L)
        when:
        def result = paymentService.getPayment(payment.paymentId, payment.clientId)
        then:
        1 * paymentRepository.findByClientIdAndPaymentId(_, _) >> Optional.of(payment)
        noExceptionThrown()
    }

    Should "throw a NotFoundException when geting a payment that does not exist"() {
        given:
        def payment = getPayment(1L, 1L)
        when:
        def result = paymentService.getPayment(payment.paymentId, payment.clientId)
        then:
        1 * paymentRepository.findByClientIdAndPaymentId(_, _) >> Optional.empty()
        thrown NotFoundException
    }

    Should "delete a payment"() {
        when:
        paymentRepository.delete(1L, 1L)
        then:
        1 * paymentRepository.delete(1L, 1L)
    }

    @Unroll
    Should "fail to update a payment (with paymentId as part of paymentRequest)  that does not exist by throwing a #exception"() {
        given:
        def paymentRequest = getPaymentRequest(paymentId, 1L, 'PLN', 'PLN1234567890123456789')
        def payment = PaymentFactory.createPayment(1L, paymentRequest)
        when:
        def result = paymentService.updatePayment(payment.clientId, paymentRequest)
        then:
        paymentRepository.findByClientIdAndPaymentId(payment.clientId, paymentId) >> paymentFound
        thrown exception
        where:
        paymentId | paymentFound     | exception
        null      | Optional.empty() | BadRequestException
        1L        | Optional.empty() | NotFoundException
    }

    Should "update a payment with paymentId as part of paymentRequest"() {
        given:
        def paymentRequest = getPaymentRequest(1L, 1L, 'PLN', 'PLN1234567890123456789')
        def payment = PaymentFactory.createPayment(1L, paymentRequest)
        when:
        def result = paymentService.updatePayment(payment.clientId, paymentRequest)
        then:
        1 * paymentRepository.findByClientIdAndPaymentId(payment.clientId, paymentRequest.paymentId) >> Optional.of(payment)
        1 * paymentRepository.save(payment) >> payment
        result == payment
    }

    @Unroll
    Should "fail to update a payment that does not exist by throwing a #exception"() {
        given:
        def paymentRequest = getPaymentRequest(1L,  1L, 'PLN', 'PLN1234567890123456789')
        def payment = PaymentFactory.createPayment(1L, paymentRequest)
        when:
        def result = paymentService.updatePayment(payment.clientId, payment.paymentId, paymentRequest)
        then:
        paymentRepository.findByClientIdAndPaymentId(payment.clientId, payment.paymentId) >> Optional.empty()
        thrown NotFoundException
    }

    Should "update a payment"() {
        given:
        def paymentRequest = getPaymentRequest(1L,  1L, 'PLN', 'PLN1234567890123456789')
        def payment = PaymentFactory.createPayment(1L, paymentRequest)
        when:
        def result = paymentService.updatePayment(payment.clientId, payment.paymentId, paymentRequest)
        then:
        1 * paymentRepository.findByClientIdAndPaymentId(payment.clientId, paymentRequest.paymentId) >> Optional.of(payment)
        1 * paymentRepository.save(payment) >> payment
        result == payment
    }
}
